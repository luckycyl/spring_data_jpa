/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50623
Source Host           : localhost:3306
Source Database       : spring_data_jpa

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2017-07-18 16:16:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for test_user
-- ----------------------------
DROP TABLE IF EXISTS `test_user`;
CREATE TABLE `test_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test_user
-- ----------------------------
INSERT INTO `test_user` VALUES ('1', '100', 'test0');
INSERT INTO `test_user` VALUES ('2', '99', 'test1');
INSERT INTO `test_user` VALUES ('3', '98', 'test2');
INSERT INTO `test_user` VALUES ('4', '97', 'test3');
INSERT INTO `test_user` VALUES ('5', '96', 'test4');
INSERT INTO `test_user` VALUES ('6', '95', 'test5');
INSERT INTO `test_user` VALUES ('7', '94', 'test6');
INSERT INTO `test_user` VALUES ('8', '93', 'test7');
INSERT INTO `test_user` VALUES ('9', '92', 'test8');
INSERT INTO `test_user` VALUES ('10', '91', 'test9');
INSERT INTO `test_user` VALUES ('11', '90', 'test10');
INSERT INTO `test_user` VALUES ('12', '89', 'test11');
INSERT INTO `test_user` VALUES ('13', '88', 'test12');
INSERT INTO `test_user` VALUES ('14', '87', 'test13');
INSERT INTO `test_user` VALUES ('15', '86', 'test14');
INSERT INTO `test_user` VALUES ('16', '85', 'test15');
INSERT INTO `test_user` VALUES ('17', '84', 'test16');
INSERT INTO `test_user` VALUES ('18', '83', 'test17');
INSERT INTO `test_user` VALUES ('19', '82', 'test18');
INSERT INTO `test_user` VALUES ('20', '81', 'test19');
INSERT INTO `test_user` VALUES ('21', '80', 'test20');
INSERT INTO `test_user` VALUES ('22', '79', 'test21');
INSERT INTO `test_user` VALUES ('23', '78', 'test22');
INSERT INTO `test_user` VALUES ('24', '77', 'test23');
INSERT INTO `test_user` VALUES ('25', '76', 'test24');
INSERT INTO `test_user` VALUES ('26', '75', 'test25');
INSERT INTO `test_user` VALUES ('27', '74', 'test26');
INSERT INTO `test_user` VALUES ('28', '73', 'test27');
INSERT INTO `test_user` VALUES ('29', '72', 'test28');
INSERT INTO `test_user` VALUES ('30', '71', 'test29');
INSERT INTO `test_user` VALUES ('31', '70', 'test30');
INSERT INTO `test_user` VALUES ('32', '69', 'test31');
INSERT INTO `test_user` VALUES ('33', '68', 'test32');
INSERT INTO `test_user` VALUES ('34', '67', 'test33');
INSERT INTO `test_user` VALUES ('35', '66', 'test34');
INSERT INTO `test_user` VALUES ('36', '65', 'test35');
INSERT INTO `test_user` VALUES ('37', '64', 'test36');
INSERT INTO `test_user` VALUES ('38', '63', 'test37');
INSERT INTO `test_user` VALUES ('39', '62', 'test38');
INSERT INTO `test_user` VALUES ('40', '61', 'test39');
INSERT INTO `test_user` VALUES ('41', '60', 'test40');
INSERT INTO `test_user` VALUES ('42', '59', 'test41');
INSERT INTO `test_user` VALUES ('43', '58', 'test42');
INSERT INTO `test_user` VALUES ('44', '57', 'test43');
INSERT INTO `test_user` VALUES ('45', '56', 'test44');
INSERT INTO `test_user` VALUES ('46', '55', 'test45');
INSERT INTO `test_user` VALUES ('47', '54', 'test46');
INSERT INTO `test_user` VALUES ('48', '53', 'test47');
INSERT INTO `test_user` VALUES ('49', '52', 'test48');
INSERT INTO `test_user` VALUES ('50', '51', 'test49');
INSERT INTO `test_user` VALUES ('51', '50', 'test50');
INSERT INTO `test_user` VALUES ('52', '49', 'test51');
INSERT INTO `test_user` VALUES ('53', '48', 'test52');
INSERT INTO `test_user` VALUES ('54', '47', 'test53');
INSERT INTO `test_user` VALUES ('55', '46', 'test54');
INSERT INTO `test_user` VALUES ('56', '45', 'test55');
INSERT INTO `test_user` VALUES ('57', '44', 'test56');
INSERT INTO `test_user` VALUES ('58', '43', 'test57');
INSERT INTO `test_user` VALUES ('59', '42', 'test58');
INSERT INTO `test_user` VALUES ('60', '41', 'test59');
INSERT INTO `test_user` VALUES ('61', '40', 'test60');
INSERT INTO `test_user` VALUES ('62', '39', 'test61');
INSERT INTO `test_user` VALUES ('63', '38', 'test62');
INSERT INTO `test_user` VALUES ('64', '37', 'test63');
INSERT INTO `test_user` VALUES ('65', '36', 'test64');
INSERT INTO `test_user` VALUES ('66', '35', 'test65');
INSERT INTO `test_user` VALUES ('67', '34', 'test66');
INSERT INTO `test_user` VALUES ('68', '33', 'test67');
INSERT INTO `test_user` VALUES ('69', '32', 'test68');
INSERT INTO `test_user` VALUES ('70', '31', 'test69');
INSERT INTO `test_user` VALUES ('71', '30', 'test70');
INSERT INTO `test_user` VALUES ('72', '29', 'test71');
INSERT INTO `test_user` VALUES ('73', '28', 'test72');
INSERT INTO `test_user` VALUES ('74', '27', 'test73');
INSERT INTO `test_user` VALUES ('75', '26', 'test74');
INSERT INTO `test_user` VALUES ('76', '25', 'test75');
INSERT INTO `test_user` VALUES ('77', '24', 'test76');
INSERT INTO `test_user` VALUES ('78', '23', 'test77');
INSERT INTO `test_user` VALUES ('79', '22', 'test78');
INSERT INTO `test_user` VALUES ('80', '21', 'test79');
INSERT INTO `test_user` VALUES ('81', '20', 'test80');
INSERT INTO `test_user` VALUES ('82', '19', 'test81');
INSERT INTO `test_user` VALUES ('83', '18', 'test82');
INSERT INTO `test_user` VALUES ('84', '17', 'test83');
INSERT INTO `test_user` VALUES ('85', '16', 'test84');
INSERT INTO `test_user` VALUES ('86', '15', 'test85');
INSERT INTO `test_user` VALUES ('87', '14', 'test86');
INSERT INTO `test_user` VALUES ('88', '13', 'test87');
INSERT INTO `test_user` VALUES ('89', '12', 'test88');
INSERT INTO `test_user` VALUES ('90', '11', 'test89');
INSERT INTO `test_user` VALUES ('91', '10', 'test90');
INSERT INTO `test_user` VALUES ('92', '9', 'test91');
INSERT INTO `test_user` VALUES ('93', '8', 'test92');
INSERT INTO `test_user` VALUES ('94', '7', 'test93');
INSERT INTO `test_user` VALUES ('95', '6', 'test94');
INSERT INTO `test_user` VALUES ('96', '5', 'test95');
INSERT INTO `test_user` VALUES ('97', '4', 'test96');
INSERT INTO `test_user` VALUES ('98', '3', 'test97');
INSERT INTO `test_user` VALUES ('99', '2', 'test98');
INSERT INTO `test_user` VALUES ('100', '1', 'test99');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '20', 'zhangsan');
INSERT INTO `user` VALUES ('2', '20', 'test1');
INSERT INTO `user` VALUES ('3', '21', 'test2');
INSERT INTO `user` VALUES ('4', '22', 'test3');
INSERT INTO `user` VALUES ('5', '20', 'test4');
INSERT INTO `user` VALUES ('6', '21', 'test5');
INSERT INTO `user` VALUES ('7', '22', 'test6');
INSERT INTO `user` VALUES ('8', '22', 'test16');
