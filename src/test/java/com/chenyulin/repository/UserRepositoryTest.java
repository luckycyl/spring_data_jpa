package com.chenyulin.repository;

import com.chenyulin.bean.User;
import org.hibernate.criterion.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 2017/7/18.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserRepositoryTest {

//####################################测试JpaSpecificationExecutor#################################################
    /*
        1 分页
        2 排序
        3 查询条件:age>50
     */
    @Test
    public void testSpecificationExecutor() {
        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "id");
        Sort sort = new Sort(order);

        Pageable pageable = new PageRequest(3, 10,sort);

        Specification<User> specification = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root,
                                         CriteriaQuery<?> criteriaQuery,
                                         CriteriaBuilder cb) {
                //将root理解为根对象 root(user(age))
                Path path = root.get("age");
                return  cb.gt(path, 50);

            }
        };


        Page<User> page = userRepository.findAll(specification, pageable);

        System.out.println("总页数：" + page.getTotalPages());
        System.out.println("总记录数：" + page.getTotalElements());
        System.out.println("第几页：" + page.getNumber());
        System.out.println("当前页面集合：" + page.getContent());
        System.out.println("当前页面的记录数：" + page.getNumberOfElements());
    }
    //####################################测试JpaRepository#################################################
    @Test
    public void testFind1() {
        User user = new User();
        user.setId(1);
        Example example = Example.of(user);
        User user1 = userRepository.findOne(example);
        System.out.println(user1);
    }

    @Test
    public void testFind(){
        User user = userRepository.findOne(23);
        System.out.println(user);
        System.out.println(userRepository.exists(15));
        System.out.println(userRepository.exists(155));
    }
    //####################################测试PagingAndSortingRepository#################################################
    @Test
    public void testPageAndSort() throws Exception {
        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "id");
        Sort sort = new Sort(order);

        Pageable pageable = new PageRequest(0, 5,sort);
        Page<User> page = userRepository.findAll(pageable);

        System.out.println("总页数：" + page.getTotalPages());
        System.out.println("总记录数：" + page.getTotalElements());
        System.out.println("第几页：" + page.getNumber());
        System.out.println("当前页面集合：" + page.getContent());
        System.out.println("当前页面的记录数：" + page.getNumberOfElements());
    }

    @Test
    public void testPage() throws Exception {
        Pageable pageable = new PageRequest(0, 5);
        Page<User> page = userRepository.findAll(pageable);

        System.out.println("总页数：" + page.getTotalPages());
        System.out.println("总记录数：" + page.getTotalElements());
        System.out.println("第几页：" + page.getNumber());
        System.out.println("当前页面集合：" + page.getContent());
        System.out.println("当前页面的记录数：" + page.getNumberOfElements());
    }

    @Test
    public void testGetCount() throws Exception {
        Integer count = userRepository.getCount();
        System.out.println(count);
    }

    @Test
    public void testGetUserListByName1() throws Exception {
        List<User> list = userRepository.getUserListByName1("test");
        System.out.println(Arrays.toString(list.toArray()));
    }

    @Test
    public void testGetUserListByNameAndAge2() throws Exception {
        List<User> list = userRepository.getUserListByNameAndAge2("test2", 21);
        System.out.println(Arrays.toString(list.toArray()));
    }

    @Test
    public void testGetUserListByNameAndAge() throws Exception {
        List<User> list = userRepository.getUserListByNameAndAge("test2", 21);
        System.out.println(Arrays.toString(list.toArray()));
    }

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testGetUserById() throws Exception {
        User user = userRepository.getUserById();
        System.out.println(user);
    }

    @Test
    public void testFindByNameAndAge() throws Exception {
        List<User> list = userRepository.findByNameAndAge("zhangsan", 21);
        System.out.println(Arrays.toString(list.toArray()));
    }

    @Test
    public void testFindByNameInAndAgeLessThan() throws Exception {
        List<String> names = new ArrayList<>();
        names.add("test1");
        names.add("test2");
        names.add("test3");
        List<User> list = userRepository.findByNameInAndAgeLessThan(names, 21);
        System.out.println(Arrays.toString(list.toArray()));
    }

    @Test
    public void testFindByNameStartingWithAndAgeLessThan() throws Exception {
        List<User> list = userRepository.findByNameStartingWithAndAgeLessThan("test", 22);
        System.out.println(Arrays.toString(list.toArray()));

    }

}