package com.chenyulin.service.serviceimpl;

import com.chenyulin.bean.User;
import com.chenyulin.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Test
    public void save() throws Exception {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setName("test"+i);
            user.setAge(100-i);
            users.add(user);
        }
        userService.save(users);
    }

    @Test
    public void delete() throws Exception {
        userService.delete(9);
    }

    @Test
    public void update() throws Exception {
        userService.update(10,1);
    }
    @Test
    public void findByName() throws Exception {
        User user=userService.findByName("xiao hua");
        Assert.assertEquals(new Integer(1),user.getId());
    }

}