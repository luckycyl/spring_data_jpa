package com.chenyulin.bean;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Administrator on 2017/7/18.
 */
@Entity
@Table(name = "test_user")
public class User implements Serializable{
    private Integer id;
    private String name;
    private Integer age;
    @Id
    @GeneratedValue
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @Column(length = 15)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
