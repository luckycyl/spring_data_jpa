package com.chenyulin.repository;

import com.chenyulin.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.Native;
import java.util.List;

/**
 * Created by Administrator on 2017/7/18.
 */
public interface UserRepository extends JpaRepository<User,Integer>,JpaSpecificationExecutor<User> {
    User findByName(String name);

    //where name like ?% and age <?
    List<User> findByNameStartingWithAndAgeLessThan(String name, Integer age);
    //where name = %? and age = %?
    List<User> findByNameAndAge(String name,Integer age);
    //where name in (?,?...) and age <?
    List<User> findByNameInAndAgeLessThan(List<String> names,Integer age);

    @Query("select o from User o where id = (select max(id) from User t1)")
    User getUserById();

    @Query("select o from User o where o.name = ?1 and o.age =?2")
    List<User> getUserListByNameAndAge(String name, Integer age);

    @Query("select o from User o where o.name = :name and o.age = :age")
    List<User> getUserListByNameAndAge2(@Param("name") String name,@Param("age") Integer age);

    @Query("select o from User o where o.name like %?1%")
    List<User> getUserListByName1(String name);

    @Query(nativeQuery = true,value = "SELECT COUNT(1) FROM user")
    Integer getCount();

    @Modifying
    @Query("update User o set o.age = ?1 where o.id = ?2")
    void update(Integer age,Integer id);

    @Modifying
    @Query("delete from User o where o.id = ?1")
    void delete(Integer id);


}
