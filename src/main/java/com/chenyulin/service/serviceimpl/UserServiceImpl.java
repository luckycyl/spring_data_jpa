package com.chenyulin.service.serviceimpl;

import com.chenyulin.bean.User;
import com.chenyulin.repository.UserRepository;
import com.chenyulin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Administrator on 2017/7/18.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    public User findByName(String name) {
        User user = userRepository.findByName(name);
        System.out.println(user);
        return user;
    }

    @Override
    @Transactional
    public void update(Integer age, Integer id) {
        userRepository.update(age, id);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        userRepository.delete(id);
    }

    @Override
    public List<User> save(List<User> users) {
        return userRepository.save(users);
    }

}
