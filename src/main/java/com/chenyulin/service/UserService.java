package com.chenyulin.service;

import com.chenyulin.bean.User;

import java.util.List;

/**
 * Created by Administrator on 2017/7/18.
 */
public interface UserService {
    User findByName(String name);

    void update(Integer age, Integer id);

    void delete(Integer id);

    List<User> save(List<User> users);
}
